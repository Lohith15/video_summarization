import openai
import os
import streamlit as st
from pydub import AudioSegment
from moviepy.editor import *
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.text_splitter import CharacterTextSplitter
from langchain.vectorstores import  FAISS
from langchain.document_loaders import UnstructuredFileLoader
from langchain.chains.summarize import load_summarize_chain
from langchain import OpenAI
from langchain.text_splitter import RecursiveCharacterTextSplitter
#from PIL import Image



def fetch_answer(question,docs):
# Text completion API
    response = openai.Completion.create(
    model="text-davinci-003",
    prompt=docs + question,
    max_tokens=100,
    temperature=0
    )
    #print(docs + question)
    return response.choices[0].text


docs = ""
@st.cache_data
def func(uploaded_file):
    video = VideoFileClip(uploaded_file.name)
    audio = video.audio
    audio.write_audiofile('test.wav')

    file_path = 'test.wav'

    segment_length = 30000

    audio = AudioSegment.from_file(file_path,format="wav")

    segments = list(range(0,len(audio),segment_length))
    k=0

    for i,start in enumerate(segments):
        end = segments[i+1] if i<len(segments) -1 else len(audio)
        segment = audio[start:end]

        segment_path = os.path.splitext(file_path)[0] + "_segment{}.wav".format(i)
        segment.export(segment_path,format="wav")
        k=k+1

   # openai.api_key = "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"
    openai.api_key = "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"

    for i in range(0,k):
        file_path = 'Test_segment{}.wav'.format(i)
        audio_file = open(file_path,"rb")
        transcript = openai.Audio.transcribe("whisper-1",audio_file)

        with open('Test.txt','a',encoding="utf-8") as f:
            transcript.text=transcript.text.encode('utf-8', errors='ignore').decode('utf-8')
            f.write(transcript.text)
            f.write(" ")
    print("Text written to file")
    
    #Resolving UTF-8 error
    with open('Test.txt', 'r',encoding="utf-8") as f:
        text = f.read()
    os.remove("Test.txt")
    text = text.encode('utf-8', 'ignore').decode('utf-8')
    with open('clean_Test.txt', 'w', encoding="utf-8") as f:
        f.write(text)

    #Refine Methodology
    loader = UnstructuredFileLoader('clean_Test.txt')
    document = loader.load()
    llm = OpenAI(openai_api_key = "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")
    char_text_splitter = RecursiveCharacterTextSplitter(chunk_size = 500, chunk_overlap = 0)
    docs = char_text_splitter.split_documents(document)
    print(len(docs))

    model = load_summarize_chain(llm = llm, chain_type="refine")
    res=model.run(docs)
    print(len(res))
    print(res)

    #return res
    with open('Main_Test.txt','w',encoding="utf-8") as f:
        f.write(res)
    
    with open('Main_Test.txt','r',encoding="utf-8") as f:
        reader = f.read()
    return reader




def display(uploaded_file):
    # Display video
    if uploaded_file is not None:
        # Read the video file as bytes
        video_bytes = uploaded_file.read()

        # Display video using Streamlit's video widget
        st.video(video_bytes, format=uploaded_file.type)
        return func(uploaded_file)
