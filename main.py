from backend import *

import streamlit as st

# Set page title
st.set_page_config(page_title="Video Summarization using GPT-3")

# Display title and description
st.title("Video summarization using GPT-3")
st.write("Upload a video and ask question")


# File uploader
uploaded_file = st.file_uploader("Upload a video file", type=["mp4", "avi", "mkv", "mov"])
reader=display(uploaded_file)
question = st.text_input("Ask Question: ", "")
submit = st.button("Submit")
if submit:
    st.write("",fetch_answer(question,reader))

